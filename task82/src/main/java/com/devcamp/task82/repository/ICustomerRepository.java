package com.devcamp.task82.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task82.entity.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Integer> {
}
